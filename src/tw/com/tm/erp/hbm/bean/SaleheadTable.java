package tw.com.tm.erp.hbm.bean;

import java.util.Date;
import java.util.List;

public class SaleheadTable implements java.io.Serializable  {

	private static final long serialVersionUID = -4312303996853625355L;
	private Long headSno;
	private Long transactionSno;
	private String posMachineCode;
	private Integer status;
	private Date salesOrderDate;
	private String shopCode;
	private Double totalActualSalesAmt;
	
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getHeadSno() {
		return headSno;
	}
	public void setHeadSno(Long headSno) {
		this.headSno = headSno;
	}
	public Long getTransactionSno() {
		return transactionSno;
	}
	public void setTransactionSno(Long transactionSno) {
		this.transactionSno = transactionSno;
	}
	public String getPosMachineCode() {
		return posMachineCode;
	}
	public void setPosMachineCode(String posMachineCode) {
		this.posMachineCode = posMachineCode;
	}
	public Date getSalesOrderDate() {
		return salesOrderDate;
	}
	public void setSalesOrderDate(Date salesOrderDate) {
		this.salesOrderDate = salesOrderDate;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Double getTotalActualSalesAmt() {
		return totalActualSalesAmt;
	}
	public void setTotalActualSalesAmt(Double totalActualSalesAmt) {
		this.totalActualSalesAmt = totalActualSalesAmt;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
