package tw.com.tm.erp.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.util.StringUtils;
import tw.com.tm.erp.hbm.bean.SoSalesOrderHead;
import tw.com.tm.erp.hbm.bean.SoSalesOrderItem;

public class ElectronicInvoiceService {
		
		private static final String localPath = "/Users/brian_macbook/ERP/InvoiceXML/"; //"D:\\InvoiceXML"
		//盟立加值平台 DEFAULT_URL
		private static final String DEFAULT_TEST_URL = "http://xmltest.551.com.tw/"; // 測試環境
		private static final String DEFAULT_URL = "http://xml.551.com.tw/";
		//=====taskCode
		private static final String A01 = "A01";//發票號碼取號
		private static final String A02 = "A02";//發票字軌取號訊息
		private static final String C0401 = "C0401";//開立發票訊息
		private static final String C0501 = "C0501";//作廢發票訊息
		private static final String C0701 = "C0701";//註銷發票訊息
		
		//=====XML element A01 發票號碼取號
		private static final String FUNCTIONCODE = "FUNCTIONCODE";
		private static final String SELLERID = "SELLERID";
		private static final String POSID = "POSID";
		private static final String POSSN = "POSSN";
		private static final String APPVSERION = "APPVSERION";
		private static final String SYSTIME = "SYSTIME";
		private static final String USERID = "USERID";
		private static final String TAXMONTH = "TAXMONTH";
		private static final String INVOICEHEADER = "INVOICEHEADER";
		private static final String INVOICESTART = "INVOICESTART";
		private static final String INVOICEEND = "INVOICEEND";
		private static final String INVOICENUMBER = "INVOICENUMBER";
		private static final String SECURITY = "SECURITY";
		private static final String SELLDETAIL = "SELLDETAIL";
		private static final String CHECKSUM = "CHECKSUM";
		//=====XML element A02 發票字軌取號訊息
		private static final String INVOICE_CODE = "INVOICE_CODE"; 
		//=====XML element C0401 開立發票訊息
		private static final String A1 = "A1"; 
		private static final String A2 = "A2"; 
		private static final String A3 = "A3"; 
		private static final String A4 = "A4"; 
		private static final String A5 = "A5"; 
		private static final String A6 = "A6"; 
		private static final String A7 = "A7"; 
		private static final String A8 = "A8"; 
		private static final String A9 = "A9"; 
		private static final String A10 = "A10"; 
		private static final String A11 = "A11"; 
		private static final String A12 = "A12"; 
		private static final String A13 = "A13"; 
		private static final String A14 = "A14";
		private static final String A15 = "A15";
		private static final String A16 = "A16";
		private static final String A17 = "A17";
		private static final String A18 = "A18";
		private static final String A19 = "A19";
		private static final String A20 = "A20";
		private static final String A21 = "A21";
		private static final String A22 = "A22";
		private static final String A23 = "A23";
		private static final String A24 = "A24";
		private static final String A25 = "A25";
		private static final String A26 = "A26";
		private static final String A27 = "A27";
		private static final String A28 = "A28";
		private static final String A29 = "A29";
		private static final String A30 = "A30";
		private static final String A31 = "A31";
		private static final String B1 = "B1";
		private static final String B2 = "B2";
		private static final String B3 = "B3";
		private static final String B4 = "B4";
		private static final String B5 = "B5";
		private static final String B6 = "B6";
		private static final String B7 = "B7";
		private static final String B8 = "B8";
		private static final String B9 = "B9";
		private static final String B10 = "B10";
		private static final String B11 = "B11";
		private static final String B12 = "B12";
		private static final String C1 = "C1";
		private static final String C2 = "C2";
		private static final String C3 = "C3";
		private static final String C4 = "C4";
		private static final String C5 = "C5";
		private static final String C6 = "C6";
		private static final String C7 = "C7";
		private static final String C8 = "C8";
		private static final String C9 = "C9";
		private static final String C10 = "C10";
		private static final String C11 = "C11";
		private static final String C12 = "C12";
		private static final String C13 = "C13";
		private static final String D1 = "D1";
		private static final String D2 = "D2";
		private static final String D3 = "D3";
		private static final String D4 = "D4";
		private static final String D5 = "D5";
	
		/**
		 * 取發票XML檔案Document（取號）
		 * @param taskCode
		 * @throws Exception 
		 */
		public Document getInvoiceXMLInfo(String taskCode, String posMachineCode) {
			System.out.println("getInvoiceXMLInfo...");
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// 建立Document物件
			Document document = DocumentHelper.createDocument();
			if(A01.equals(taskCode)) {
				document.addComment(" 每日開機 POS機送出 ");
				Element root = document.addElement("INDEX");
			    root.addElement(FUNCTIONCODE).setText(A01);
			    root.addElement(SELLERID).setText("51877398");
			    root.addElement(POSID).setText(posMachineCode);
			    root.addElement(POSSN).setText("b954967f7ba903740806");// 99 ==== b954967f7ba903740806
			    root.addElement(APPVSERION);
			    root.addElement(SYSTIME).setText(sdf.format(date));
			    root.addElement(USERID);
			    root.addElement(TAXMONTH);
			    root.addElement(INVOICEHEADER);
			    root.addElement(INVOICESTART);
			    root.addElement(INVOICEEND);
			    root.addElement(INVOICENUMBER);
			    root.addElement(SECURITY);
			    root.addElement(SELLDETAIL);
			    root.addElement(CHECKSUM);
			} else if(A02.equals(taskCode)) {
				Element root = document.addElement("Invoice");
				root.addElement(INVOICE_CODE).setText(taskCode);
				root.addElement(SELLERID).setText("51877398");
				root.addElement(POSID).setText("001");
			    root.addElement(POSSN).setText("71fa6989f71b38e44966");
			    root.addElement(SYSTIME).setText(sdf.format(date));
			} 
			return document;
		}
		
	/**
	 * 取發票XML檔案Document（交易）
	 * @param taskCode
	 * @throws Exception 
	 */
	public Document getTransactionInvoiceXMLInfo(String taskCode, SoSalesOrderHead soSalesOrderHead) {
		System.out.println("getTransactionInvoiceXMLInfo...");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf3 = new SimpleDateFormat("HH:mm:ss");
		// 建立Document物件
		Document document = DocumentHelper.createDocument();
		if(A01.equals(taskCode)) {
			document.addComment(" 每日開機 POS機送出 ");
			Element root = document.addElement("INDEX");
		    root.addElement(FUNCTIONCODE).setText(A01);
		    root.addElement(SELLERID).setText("51877398");
		    root.addElement(POSID).setText("001");
		    root.addElement(POSSN).setText("71fa6989f71b38e44966");
		    root.addElement(APPVSERION);
		    root.addElement(SYSTIME).setText(sdf.format(date));
		    root.addElement(USERID);
		    root.addElement(TAXMONTH);
		    root.addElement(INVOICEHEADER);
		    root.addElement(INVOICESTART);
		    root.addElement(INVOICEEND);
		    root.addElement(INVOICENUMBER);
		    root.addElement(SECURITY);
		    root.addElement(SELLDETAIL);
		    root.addElement(CHECKSUM);
		} else if(A02.equals(taskCode)) {
			Element root = document.addElement("Invoice");
			root.addElement(INVOICE_CODE).setText(taskCode);
			root.addElement(SELLERID).setText("51877398");
			root.addElement(POSID).setText(soSalesOrderHead.getPosMachineCode());
		    root.addElement(POSSN).setText("71fa6989f71b38e44966");
		    root.addElement(SYSTIME).setText(sdf.format(date));
		} else if(C0401.equals(taskCode)) {
			List<SoSalesOrderItem> soSalesOrderItemList = soSalesOrderHead.getSoSalesOrderItems();
			Element root = document.addElement("Invoice");
			root.addElement(A1).setText(taskCode);//訊息類型
			root.addElement(A2).setText("WW67435550");//發票號碼
			root.addElement(A3).setText(sdf2.format(date));//發票開立日期
			root.addElement(A4).setText(sdf3.format(date));//發票開立時間
			if(StringUtils.hasText(soSalesOrderHead.getBuyerId())) {
				//buyer 識別碼(統一編號)
				root.addElement(A5).setText(soSalesOrderHead.getBuyerId());
			}else {
				root.addElement(A5).setText("0000000000");
			}
			root.addElement(A6).setText("0000");//buyer名稱
			root.addElement(A7);
			root.addElement(A8);
			root.addElement(A9);
			root.addElement(A10);
			root.addElement(A11);
			root.addElement(A12);
			root.addElement(A13);
			root.addElement(A14);
			root.addElement(A15);
			root.addElement(A16);
			root.addElement(A17);
			root.addElement(A18);
			root.addElement(A19).setText("2013-02-27");
			root.addElement(A20).setText("資國");
			root.addElement(A21).setText("1020001054");
			root.addElement(A22);//發票類別
			root.addElement(A23);
			root.addElement(A24).setText("0");//捐贈註記
			root.addElement(A28).setText("Y");//紙本電子發票已列印註記
			root.addElement(A29);
			root.addElement(A30).setText("7124"); //四位數隨機碼
			root.addElement(A31);
			if(soSalesOrderItemList!=null && soSalesOrderItemList.size()>0) {
				for(int i=0; i<soSalesOrderItemList.size(); i++) {
					SoSalesOrderItem soSalesOrderItem = soSalesOrderItemList.get(i);
					Element bTab = root.addElement("B");
					bTab.addElement(B1).setText(String.valueOf(soSalesOrderItem.getIndexNo()));//商品項目資料
					bTab.addElement(B2).setText(soSalesOrderItem.getItemCName());//品名
					bTab.addElement(B3).setText(String.valueOf(Math.round(soSalesOrderItem.getQuantity())));//數量
					bTab.addElement(B4).setText(soSalesOrderItem.getSalesUnit());
					bTab.addElement(B5).setText(String.valueOf(Math.round(soSalesOrderItem.getActualSalesAmount())));//單價
					bTab.addElement(B6).setText(String.valueOf(Math.round(soSalesOrderItem.getActualSalesAmount())));//金額
					bTab.addElement(B7).setText(String.valueOf(soSalesOrderItem.getIndexNo()));//明細排列序號
					bTab.addElement(B8);
					bTab.addElement(B9);
					bTab.addElement(B10);
					bTab.addElement(B11).setText(soSalesOrderItem.getItemCode());
					bTab.addElement(B12);
				}	
			}
			root.addElement(C1).setText(String.valueOf(Math.round(soSalesOrderHead.getTotalActualSalesAmount()*0.05)));//應稅銷售額合計(新台幣)
			root.addElement(C2).setText("0");//免稅銷售額合計(新台幣)
			root.addElement(C3).setText("0");;//零稅率銷售額合計(新台幣)
			root.addElement(C4).setText("1");;//課稅別
			root.addElement(C5).setText("0.05");;//稅率
			root.addElement(C6).setText("0");;//營業稅額
			root.addElement(C7).setText(String.valueOf(Math.round(soSalesOrderHead.getTotalActualSalesAmount()*0.05)));//總計
			root.addElement(C8);
			root.addElement(D1).setText("51877398");//seller 識別碼(統一編號)
			root.addElement(D2).setText("71fa6989f71b38e44966");//sellerPOSSN(POS 機出廠序號)(通道金鑰)
			root.addElement(D3).setText("001");//POSID(POS 機編號)
			root.addElement(D4).setText(sdf.format(date));//XML 產生時間
		}
		return document;
	}
	/**
	 * 產實際XML檔案
	 * @param documentInfo
	 * @param filePath
	 */
	public static void createXMLFile(Document documentInfo, File filePath) {
		// 建立輸出格式(OutputFormat物件)
		  OutputFormat format = OutputFormat.createPrettyPrint();
		  try {
			   if(!filePath.getParentFile().exists()) {
					filePath.getParentFile().mkdirs();
				}
				if (!filePath.exists()) {
					filePath.createNewFile();
				}else {
					System.out.println("file 已存在!!");
					throw new Exception("file 已存在~~");
				}
			   // 建立XMLWriter物件
			   XMLWriter writer = new XMLWriter(new FileOutputStream(filePath), format);
			   //設定不自動進行轉義
			   writer.setEscapeText(false);
			   // 生成XML檔案
			   writer.write(documentInfo);
			   //關閉XMLWriter物件
			   writer.close();
			   System.out.println("輸出 XML 完成!!");
			   
		  } catch (IOException e) {
			   e.printStackTrace();
		  } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根據不同taskCode執行發票(取號、上傳等)作業
	 * @param taskCode
	 * @param filePath xml檔案路徑
	 * @return
	 * @throws IOException
	 */
	public static String executeInvoiceCallAPI(String taskCode, Document documentInfo) throws IOException{
		System.out.println("executeInvoiceCallAPI...");
		
		String response = "";
		HttpURLConnection connection = null;
		int responseCode = 0;
		try {
			SimpleDateFormat sdfName = new SimpleDateFormat("yyyyMMddHHmmss");
			String reqName = taskCode+"_"+sdfName.format(new Date())+".xml";
			String resName = "RES_"+reqName;
			File filePath = new File(localPath+reqName);
			//先產xml檔案
			createXMLFile(documentInfo, filePath);
			
			//step1 建立連線
			URL url = new URL(DEFAULT_TEST_URL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");// POST,GET,POST,DELETE,INPUT
			connection.setRequestProperty("content-type", "text/xml;charset=UTF-8");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			
			//step2 讀出xml檔轉xml為String
			String xmlStr = "";
			SAXReader reader = new SAXReader();
			Document document = reader.read(filePath);
			xmlStr = document.asXML();
			System.out.println("=======讀出的XML內容如下:======");
			System.out.println(document.asXML());
			
			//step3 寫入xmlStr並輸出
			try(OutputStream os = connection.getOutputStream()) {
				byte[] input = xmlStr.getBytes("utf-8");
				os.write(input, 0, input.length);			
			}
			
			responseCode = connection.getResponseCode();
			System.out.println("連線狀態:"+responseCode);
			
			//step4 取得輸入的回應轉字串
			if(200 == responseCode) {
				System.out.println("Success!");
				//獲得輸入,將位元組流轉換為字元流
			    InputStream is = connection.getInputStream();  
			    InputStreamReader isr = new InputStreamReader(is, "utf-8");  
			    //使用快取區
			    BufferedReader br = new BufferedReader(isr);  
			    
			    StringBuffer sb = new StringBuffer();
			    String temp = null;  
			    while ((temp = br.readLine()) != null) {  
			    	sb.append(temp);  
			    } 
			    
			    response = sb.toString().trim();//回應資料轉字串
			    
			    Document documentRes = DocumentHelper.parseText(response);
				createXMLFile(documentRes,new File(localPath+resName));
			    
			    //關閉bufferReader和輸入流
			    br.close();  
			    isr.close();  
			    is.close();  
			    is = null;
			}
		} catch (IOException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return response;
	}
	
	/**
	 * res中擷取兩個字串之間的值
	 * @param res
	 * @param strStart
	 * @param strEnd
	 * @return
	 */
	public String subString(String res, String strStart, String strEnd) {

        //找出指定的2個字元在 該字串裡面的 位置
        int strStartIndex = res.indexOf(strStart);
        int strEndIndex = res.indexOf(strEnd);

        //index 為負數 即表示該字串中 沒有該字元
        if (strStartIndex < 0) {
            return null;//"不存在: " + strStart + ", 無法擷取目標字串";
        }
        if (strEndIndex < 0) {
            return null;//"不存在: " + strEnd + ", 無法擷取目標字串";
        }
        //開始擷取
        String result = res.substring(strStartIndex, strEndIndex).substring(strStart.length());
        return result;
    }
	
	public static void main (String[] args) throws IOException, DocumentException{
		
		String taskCode = A01;
		String posMachineCode = "001";
		
		SoSalesOrderHead soSalesOrderHead = new SoSalesOrderHead();
	
		List<SoSalesOrderItem> soSalesOrderItemList = new ArrayList<SoSalesOrderItem>();
		SoSalesOrderItem soSalesOrderItem = new SoSalesOrderItem();
		soSalesOrderItem.setItemCode("1QA20099920");
		soSalesOrderItem.setItemCName("愛麗絲夢遊仙境11");
		soSalesOrderItem.setSalesUnit("本");
		soSalesOrderItem.setQuantity(1D);
		soSalesOrderItem.setActualSalesAmount(3740D);
		soSalesOrderItem.setIndexNo(1L);
		SoSalesOrderItem soSalesOrderItem2 = new SoSalesOrderItem();
		soSalesOrderItem2.setItemCode("3759579284QAP");
		soSalesOrderItem2.setItemCName("雲遊四海22");
		soSalesOrderItem2.setSalesUnit("瓶");
		soSalesOrderItem2.setQuantity(1D);
		soSalesOrderItem2.setActualSalesAmount(3740D);
		soSalesOrderItem2.setIndexNo(2L);
		
		soSalesOrderItemList.add(soSalesOrderItem);
		soSalesOrderItemList.add(soSalesOrderItem2);
		
		soSalesOrderHead.setSoSalesOrderItems(soSalesOrderItemList);
		soSalesOrderHead.setTotalActualSalesAmount(7484D);
		
//		SimpleDateFormat sdfName = new SimpleDateFormat("yyyyMMddHHmmss");
//		String reqName = taskCode+"_"+sdfName.format(new Date())+".xml";
//		
//		Document document = null;
//		//取得xml Document資訊
//		if(taskCode.equals(A01) || taskCode.equals(A02)) {
//			document = getInvoiceXMLInfo(taskCode, posMachineCode);
//		}else if (taskCode.equals(C0401)) {
//			document = getTransactionInvoiceXMLInfo(taskCode, soSalesOrderHead);
//		}
//		//產實體xml並執行送出xml
//		String res = executeInvoiceCallAPI(taskCode, document);
//		
//		System.out.println("回應:"+res);
//		
//		Document documentRes = DocumentHelper.parseText(res);
//		createXMLFile(documentRes,new File("/Users/brian_macbook/ERP/InvoiceXML/RES_"+reqName));
//		
//		System.out.println("=================");
//		System.out.println("FUNCTIONCODE: "+subString(res, "<FUNCTIONCODE>", "</FUNCTIONCODE>"));
//		System.out.println("MESSAGE: "+subString(res, "<MESSAGE>", "</MESSAGE>"));
//		System.out.println("ERROR_CODE: "+subString(res, "<ERROR_CODE>", "</ERROR_CODE>"));
//		System.out.println("INVOICEHEADER: "+subString(res, "<INVOICEHEADER>", "</INVOICEHEADER>"));
//		System.out.println("INVOICESTART: "+subString(res, "<INVOICESTART>", "</INVOICESTART>"));
//		System.out.println("INVOICEEND: "+subString(res, "<INVOICEEND>", "</INVOICEEND>"));
//		
		
		
	}

}
