package tw.com.tm.erp.test;

import org.springframework.context.ApplicationContext;

import tw.com.tm.erp.action.SoSalesOrderMainAction;
import tw.com.tm.erp.hbm.SpringUtils;

public class ElectronicInvoiceSendJob {
	private static ApplicationContext context = SpringUtils.getApplicationContext();
	
	public void electronicInvoiceUpload()throws Exception{
		
		ElectronicInvoiceService electronicInvoiceService = (ElectronicInvoiceService) context.getBean("electronicInvoiceService");
		electronicInvoiceService.executeInvoiceCallAPI("C0401", null);
	}
}
