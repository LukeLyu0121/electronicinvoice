package tw.com.tm.erp.test;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import tw.com.tm.erp.hbm.bean.ImItem;
import tw.com.tm.erp.hbm.bean.SoSalesOrderHead;
import tw.com.tm.erp.hbm.bean.SoSalesOrderItem;
import tw.com.tm.erp.hbm.dao.ImItemDAO;

public class Print implements Printable {
	
	public int pageSize;//列印的總頁數
	public double paperW=0;//列印的紙張寬度
	public double paperH=0;//列印的紙張高度
	public SoSalesOrderHead head = new SoSalesOrderHead();
	private ImItemDAO imItemDAO;
    public void setImItemDAO(ImItemDAO imItemDAO) {
    	this.imItemDAO = imItemDAO;
    }
	
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex >= pageSize) 
			return Printable.NO_SUCH_PAGE;//退出列印
		else {
			Graphics2D g2 = (Graphics2D) graphics;
			g2.setColor(Color.BLUE);
			Paper p = new Paper();
			p.setImageableArea(0, 0, 230, 450);
			p.setSize(230, 450);// 設置紙張的大小
			pageFormat.setPaper(p);
			drawCurrentPageText(g2, pageFormat);//調用列印內容的方法
			return PAGE_EXISTS;
		}
	}


	// 列印內容
	private void drawCurrentPageText(Graphics2D g2, PageFormat pf) {
		
		try {
			 Graphics2D graphics2D = (Graphics2D) g2;
			 int font_distence = 5;
			 int font_height = 10;
			 int yIndex = 30;
			 
			 Font font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 drawString(graphics2D, "采盟股份有限公司", 10, 15, 220, font_height+font_distence);//font_height+font_distence
			    
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"台北市復興北路57號3F之1,2,7" , 10, 30, 220, font_height+font_distence);
			    
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"#:" , 10, 45, 100, font_height+font_distence);
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"Tel:0227815999" , 100, 45, 140, font_height+font_distence);
			 
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 String no = "PFD99091200023";
			 if(null != this.head && null !=this.head.getOrderDiscountType()) {
				 no = head.getOrderDiscountType();
			 }
			 yIndex = drawString(graphics2D,no , 10, 60, 180, font_height+font_distence);
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"P:2" , 140, 60, 180, font_height+font_distence);
			 
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"S/N:DOA0000004" , 10, 75, 180, font_height+font_distence);
			 
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"PMO:"+head.getPromotionCode() , 10, 90, 180, font_height+font_distence);
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,"SD:0" , 140, 90, 180, font_height+font_distence);
			 
			 Color defaultColor = graphics2D.getColor();
			 Color grey = new Color(145, 145, 145);
			 List<SoSalesOrderItem> items = head.getSoSalesOrderItems();
			 System.out.println(items.size());
			 
			//邊框
			 Stroke stroke = new BasicStroke(2f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL,0,new float[]{4, 4},0);
			 graphics2D.setStroke(stroke);
			 graphics2D.drawRect(10, 95, 210, (90+(items.size())*45));

			 double totalDis = 0.0D;
			 //detail
			 for(int i=0;i<items.size();i++) {
				totalDis+=(null==items.get(i).getDiscount()?0.0D:items.get(i).getDiscount());
			 	printItem(graphics2D,yIndex,15,70+((i+1)*45),items.get(i));
			 }
			 
			 //TOTAL 
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 drawString(graphics2D, "SUB TOTAL:", 15, (130+(items.size())*45), 220, font_height+font_distence);//font_height+font_distence
			    
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,String.valueOf(head.getTotalActualSalesAmount()) , 160, (145+(items.size())*45), 220, font_height+font_distence);
			 
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 drawString(graphics2D, "TOTAL DISCOUNT:", 15, (160+(items.size())*45), 220, font_height+font_distence);//font_height+font_distence
			    
			 font = new Font(Font.DIALOG, Font.BOLD, 14);
			 graphics2D.setFont(font);
			 yIndex = drawString(graphics2D,String.valueOf(totalDis) , 175, (175+(items.size())*45), 220, font_height+font_distence);
			  
			    graphics2D.setColor(defaultColor);
			    graphics2D.setFont(new Font("宋體", Font.PLAIN, 6));
			    graphics2D.setColor(grey);
			    yIndex = yIndex +  20;
			    //graphics2D.drawLine(0, yIndex, 140, yIndex);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void printItem(Graphics2D graphics2D,int yIndex,int startX,int startY,SoSalesOrderItem item) {
		int font_height = 10;
		int font_distence = 5;
		try {
			//ImItemDAO imItemDAO = new ImItemDAO();
			//ImItem imItem = imItemDAO.findItem("T2", item.getItemCode());
			Font font = new Font(Font.DIALOG, Font.BOLD, 14);
	 		graphics2D.setFont(font);
			yIndex = drawString(graphics2D,item.getItemCode() , startX, startY, 80, font_height+font_distence);
			font = new Font(Font.DIALOG, Font.BOLD, 14);
			graphics2D.setFont(font);
			yIndex = drawString(graphics2D,item.getItemCode() , startX+100, startY, 80, font_height+font_distence);
			
			font = new Font(Font.DIALOG, Font.BOLD, 14);
			graphics2D.setFont(font);
			yIndex = drawString(graphics2D,String.valueOf(item.getOriginalUnitPrice()) , startX, startY+15, 50, font_height+font_distence);
			font = new Font(Font.DIALOG, Font.BOLD, 14);
			graphics2D.setFont(font);
			yIndex = drawString(graphics2D,String.valueOf(item.getQuantity()) , startX+70, startY+15, 50, font_height+font_distence);
			font = new Font(Font.DIALOG, Font.BOLD, 14);
			graphics2D.setFont(font);
			yIndex = drawString(graphics2D,item.getActualSalesAmount()+" TX" , startX+140, startY+15, 50, font_height+font_distence);
			
			font = new Font(Font.DIALOG, Font.BOLD, 14);
			graphics2D.setFont(font);
			yIndex = drawString(graphics2D,"DISCOUNT" , startX, startY+30, 80, font_height+font_distence);
			font = new Font(Font.DIALOG, Font.BOLD, 14);
			graphics2D.setFont(font);
			yIndex = drawString(graphics2D,String.valueOf(item.getDiscountRate()) , startX+100, startY+30, 80, font_height+font_distence);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//連接列印機，彈出列印對話框
	public void starPrint() {
		try {
			PrinterJob prnJob = PrinterJob.getPrinterJob();
			PageFormat pageFormat = new PageFormat();
			pageFormat.setOrientation(PageFormat.PORTRAIT);
			prnJob.setPrintable(this);
			//彈出列印對話框，也可以選擇不彈出列印提示框，直接列印
			//if (!prnJob.printDialog())
			//return;
			//獲取所連接的目標列印機的進紙規格的寬度，單位：1/72(inch)
			paperW=prnJob.getPageFormat(null).getPaper().getWidth();
			System.out.println("paperW:"+paperW);
			//獲取所連接的目標列印機的進紙規格的寬度，單位：1/72(inch)
			paperH=prnJob.getPageFormat(null).getPaper().getHeight();
			System.out.println("paperH:"+paperH);
			//System.out.println("paperW:"+paperW+";paperH:"+paperH);
			prnJob.print();//啟動列印工作
		} catch (PrinterException ex) {
			ex.printStackTrace();
			System.err.println("列印錯誤：" + ex.toString());
		}
	}
	//入口方法
	public static void main(String[] args) {
		
		SoSalesOrderHead head = new SoSalesOrderHead();
		head.setCustomerCode("CustomerCode");
		head.setGuiCode("GuiCode");
		head.setSuperintendentCode("T49674");
		head.setSalesOrderDate(new Date());
		head.setPromotionCode("PRO100");
		head.setTotalActualSalesAmount(10000.0D);
		
		List<SoSalesOrderItem> soSalesOrderItems = new ArrayList<SoSalesOrderItem>();
		SoSalesOrderItem item = new SoSalesOrderItem();
		item.setItemCName("123");
		item.setItemCode("123");
		item.setDiscount(100.0D);
		item.setActualSalesAmount(21.0D);
		item.setOriginalUnitPrice(11.0D);
		item.setQuantity(1.0D);
		item.setDiscountRate(99.0D);
		SoSalesOrderItem item2 = new SoSalesOrderItem();
		item2.setItemCName("321");
		item2.setItemCode("321");
		item2.setDiscount(200.0D);
		item2.setActualSalesAmount(32.0D);
		item2.setOriginalUnitPrice(31.0D);
		item2.setQuantity(2.0D);
		item2.setDiscountRate(99.0D);
		soSalesOrderItems.add(item);
		soSalesOrderItems.add(item2);
		head.setSoSalesOrderItems(soSalesOrderItems);
		
		Print pm = new Print();// 實例化列印類
		pm.pageSize = 1;//列印兩頁
		pm.head = head;
		pm.starPrint();
	}
	
	/**
	* 字串輸出
	* @param graphics2D  畫筆
	* @param text     列印文字
	* @param x       列印起點 x 座標
	* @param y       列印起點 y 座標
	* @param lineWidth   行寬
	* @param lineHeight  行高
	* @return 返回終點 y 座標
	*/
	private static int drawString(Graphics2D graphics2D, String text, int x, int y, int lineWidth, int lineHeight){
		FontMetrics fontMetrics = graphics2D.getFontMetrics();
		System.out.println(fontMetrics);
		if(fontMetrics.stringWidth(text)<lineWidth){
			graphics2D.drawString(text, x, y);
			return y;
		} else{
			char[] chars = text.toCharArray();
			int charsWidth = 0;
			StringBuffer sb = new StringBuffer();
			for (int i=0; i<chars.length; i++  ){
				if((charsWidth  + fontMetrics.charWidth(chars[i]))>lineWidth){
					graphics2D.drawString(sb.toString(), x, y);
					sb.setLength(0);
					y = y  + lineHeight;
					charsWidth = fontMetrics.charWidth(chars[i]);
					sb.append(chars[i]);
				} else{
					charsWidth = charsWidth  + fontMetrics.charWidth(chars[i]);
					sb.append(chars[i]);
				}
			}
			if(sb.length()>0){
				graphics2D.drawString(sb.toString(), x, y);
				y = y +  lineHeight;
			}
			return y - lineHeight;
		}
	}
	
	
	// 根據str,font的樣式以及輸出檔案目錄
    public static void createImage(String str, Font font, File outFile,Integer width, Integer height) throws Exception {
	    // 建立圖片
	    BufferedImage image = new BufferedImage(width, height,
	    BufferedImage.TYPE_INT_BGR);
	    Graphics g = image.getGraphics();
	    g.setClip(0, 0, width, height);
	    g.setColor(Color.black);
	    g.fillRect(0, 0, width, height);// 先用黑色填充整張圖片,也就是背景
	    g.setColor(Color.red);// 在換成黑色
	    g.setFont(font);// 設定畫筆字型
	    /** 用於獲得垂直居中y */
	    Rectangle clip = g.getClipBounds();
	    FontMetrics fm = g.getFontMetrics(font);
	    int ascent = fm.getAscent();
	    int descent = fm.getDescent();
	    int y = (clip.height - (ascent + descent)) / 2 + ascent;
	    for (int i = 0; i < 6; i++) {// 256 340 0 680
	    g.drawString(str, i * 680, y);// 畫出字串
	    }
	    g.dispose();
	    ImageIO.write(image, "png", outFile);// 輸出png圖片
    }
}
