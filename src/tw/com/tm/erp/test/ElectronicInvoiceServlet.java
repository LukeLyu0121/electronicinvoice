package tw.com.tm.erp.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.dom4j.Document;
import com.google.gson.Gson;
import tw.com.tm.erp.hbm.SpringUtils;
import tw.com.tm.erp.hbm.bean.SoSalesOrderHead;


@WebServlet("/ElectronicInvoiceServlet")
public class ElectronicInvoiceServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 470877352747235748L;
	
	private ElectronicInvoiceService electronicInvoiceService;

	public ElectronicInvoiceServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		System.out.println("doGet...");
		request.setCharacterEncoding("utf-8");
		
		try {
			
			SoSalesOrderHead soSalesOrderHead = null; 
			String json = "";
			Document document = null;
			Gson gson = new Gson();
			
			
			electronicInvoiceService = (ElectronicInvoiceService)SpringUtils.getApplicationContext().getBean("electronicInvoiceService");
			
			String requestJsonStr = readJSONString(request);
			System.out.println(requestJsonStr);
			
			ElectronicInvoiceRequestBean<ElectronicInvoiceRequestBean> electronicInvoiceRequestBean = gson.fromJson(requestJsonStr, ElectronicInvoiceRequestBean.class);
			
			String taskCode = electronicInvoiceRequestBean.getTaskCode();
			String posMachineCode = electronicInvoiceRequestBean.getPosMachineCode();
			
			if("A01".equals(taskCode)) {
				document = electronicInvoiceService.getInvoiceXMLInfo("A01", posMachineCode);
			}else if ("C0401".equals(taskCode)) {
				document = electronicInvoiceService.getTransactionInvoiceXMLInfo("C0401", soSalesOrderHead);
			}
			
			json = ElectronicInvoiceService.executeInvoiceCallAPI(electronicInvoiceRequestBean.getTaskCode(), document);
			
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			System.out.println(json);
			System.out.println("=================");
			System.out.println("FUNCTIONCODE: "+electronicInvoiceService.subString(json, "<FUNCTIONCODE>", "</FUNCTIONCODE>")+
					",MESSAGE: "+electronicInvoiceService.subString(json, "<MESSAGE>", "</MESSAGE>")+
					",ERROR_CODE: "+electronicInvoiceService.subString(json, "<ERROR_CODE>", "</ERROR_CODE>")+
					",INVOICEHEADER: "+electronicInvoiceService.subString(json, "<INVOICEHEADER>", "</INVOICEHEADER>")+
					",INVOICESTART: "+electronicInvoiceService.subString(json, "<INVOICESTART>", "</INVOICESTART>")+
					",INVOICEEND: "+electronicInvoiceService.subString(json, "<INVOICEEND>", "</INVOICEEND>"));
			
			ElectronicInvoiceResponseBean electronicInvoiceResponseBean = new ElectronicInvoiceResponseBean();
			electronicInvoiceResponseBean.setInvoiceHead(electronicInvoiceService.subString(json, "<INVOICEHEADER>", "</INVOICEHEADER>"));
			electronicInvoiceResponseBean.setInvoiceNumber(electronicInvoiceService.subString(json, "<INVOICESTART>", "</INVOICESTART>"));
			String requestStr = gson.toJson(electronicInvoiceResponseBean);
			
			out.println(requestStr);
			out.flush();
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String readJSONString(HttpServletRequest request) {
		StringBuffer json = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				json.append(line);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return json.toString();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		this.doGet(request, response);
	}

	@Override
	public void destroy() {
		super.destroy();
	}

	@Override
	public void init() throws ServletException {
		
	}
	
	
}
